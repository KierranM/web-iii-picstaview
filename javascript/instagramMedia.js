/*
	Filename: instagramMedia.js
	Purpose: Provides a nice way of accessing instagram
	Date: 30/03/2014
	Author: Kierran McPherson
*/

//Declare the InstagramMedia constructor
//	- rawData -> The raw JSON object data of an instagram media object
function InstagramMedia (rawData) {
	//Create a that variable to get around scoping issues
	var that = this;

	//Store the raw data in case something wants it
	this.rawData = rawData;

	//A method for checking if this InstagramMedia object is equal to another
	this.isEqualTo = isEqualTo;

	//Pull apart the raw data into fields for the media object
	this.tags = rawData.tags;
	this.mediaID = rawData.id;
	this.timeTaken = rawData.created_time;
	this.humanReadableTime = createHumanTime(this.timeTaken);
	this.link = rawData.link;

	this.latitude = rawData.location.latitude;
	this.longitude = rawData.location.longitude;
	this.locationName = rawData.location.name; //Ths may be 

	this.comments = [];
	//Load the comments
	for(var i = 0; i < rawData.comments.data.length; i++){
		this.comments.push({
			"commentTime": rawData.comments.data[i].created_time,
			"commentHRTime": createHumanTime(rawData.comments.data[i].created_time),
            "commentText": rawData.comments.data[i].text,
            "username": rawData.comments.data[i].from.username,
            "userID": rawData.comments.data[i].from.id,
            "fullName": rawData.comments.data[i].from.full_name,
            "commentID": rawData.comments.data[i].id
		});
	}
	this.likes = [];
	//Load the likes
	for(var i = 0; i < rawData.likes.data.length; i++){
		this.likes.push({
			"username": rawData.likes.data[i].username,
            "likerID": rawData.likes.data[i].id,
            "fullName": rawData.likes.data[i].full_name
		});
	}

	this.thumbnail = rawData.images.thumbnail;
	this.image = rawData.images.standard_resolution;
	this.filterUsed = rawData.filter;

	this.usersInPhoto = [];
	for (var i = 0; i < rawData.users_in_photo.length; i++) {
		this.usersInPhoto.push({
            "yPos": rawData.users_in_photo[i].position.x,
            "xPos": rawData.users_in_photo[i].position.y,
            "username": rawData.users_in_photo[i].user.username,
            "userID": rawData.users_in_photo[i].user.id,
            "full_name": rawData.users_in_photo[i].user.full_name
		});
	}
	if(rawData.caption !== undefined && rawData.caption !== null)
	{
		this.caption = rawData.caption.text;
	} else {
		this.caption = null;
	}
	this.user = {
		"username": rawData.user.username,
		"fullName": rawData.user.full_name,
        "bio": rawData.user.bio,
        "userID": rawData.user.id
	};

	//Checks if this InstagramMedia object is the same as 
	// - otherMediaObject -> The media object we are checking against
	function isEqualTo (otherMediaObject) {
		if(that.mediaID === otherMediaObject.mediaID)
		{
			return true;
		} else {
			return false;
		}
	}

	//Creates a human readable time string from the unix timestamp
	function createHumanTime (timestamp) {
		var a = new Date(timestamp*1000);

		return a.toLocaleString();
	}

	//Adds a zero in front of a single digit number
	function precedingZero (number) {
		var ret = "";
		if (number < 10) {
			ret = "0" + number;
		} else {
			ret = number;
		}

		return ret;
	}
}