/*
	Filename: instagramAccessor.js
	Purpose: Provides methods for retrieving information from the Instagram API
	Date: 25/03/2014
	Author: Kierran McPherson
	Dependencies:
		InstagramMedia.js
*/

//Declare the InstagramAccessor constructor
//	clientID = The clientID of the application (From registration with Instagram)
//	accessToken = The accessToken of the user, retrieved from the URL GET variable after a User has authenticated with Instagram (Optional)
function InstagramAccessor (clientID, accessToken) {
	//Store this as that (javascript scoping issue)
	var that = this;

	//Store the clientID
	this.clientID = clientID;
	//Will be undefined if the accessToken is omitted
	this.accessToken = accessToken;

	//The arrary that stores the last search results
	this.lastResults = [];

	//Searches for media with the given information
	this.searchForMedia = performMediaSearch;

	//Looks through an array of InstagraMedia objects and removes duplicates
	
	this.removeDuplicates = removeDuplicates;
	this.filterHashtags = filterHashtags;
	this.filterUsernames = filterUsers;

	//Makes a request to the InstagramAPI requesting media using the given search options
	//stores the results in the lastSearch variable
	// - callback -> Required: The function to call when the results have been returned
	// - latitude -> Required: The latitude in degrees (90 to -90)
	// - longitude -> Required: The longitude in degrees (180 to -180) 
	// - maxTimeStamp -> Optional: The maximum timestamp allowed for results. Defaults to the current unix timestamp
	// - minTimeStamp -> Optional: The minimum timestamp allowed for results. Defaults to the last 7 days
	// - distance -> Optional: The distance around the location given by the lat-long to return results for. Defaults to 1000
	function performMediaSearch (callback, latitude, longitude, maxTimeStamp, minTimeStamp, distance, hashtags, usernames) {
		//Create the arinitial query with no arguments other than the client id
		var query = "https://api.instagram.com/v1/media/search?client_id=" + that.clientID;

		//If the accessToken has been set, then add it to the query
		if(that.accessToken !== undefined && that.accessToken !== null){
			query += "&access_token=" + that.accessToken;
		}

		//Add the latitude and longitude to the query
		query += "&lat=" + latitude + "&lng=" + longitude;

		//If the maxTimeStamp has been defined, add it to the query
		if(maxTimeStamp !== undefined && maxTimeStamp !== null) { 
			query += "&max_timestamp=" + maxTimeStamp;
		}
		else{
			query += "&max_timestamp=" + Math.round(new Date().getTime() / 1000);
		}

		//If the minTimeStamp has been defined, add it to the query
		if (minTimeStamp !== undefined && minTimeStamp !== null) {
			query += "&min_timestamp=" + minTimeStamp;
		}
		else{
			query += "&min_timestamp=" + maxTimeStamp - (((7 * 24)*60)*60);
		}

		//If the distance has been defined, add it to the query
		if (distance !== undefined && distance !== null) {

			query += "&distance=" + distance;
		}
		else{
			query += "&distance=" + 1000;
		}

		query += "&count=50";

		query += "&callback=?";

		//Create an array to hold the results
		var results = [];

		$.getJSON(query, processData);

		function processData (jsonData, textStatus) {
			//Get the data field from the jsonResult
			var data = jsonData.data;

			//Make sure data was returned
			if (data !== undefined) {
				//Loop for every item in the data array
				for(var i = 0; i < data.length; i++){
					//create the InstagramMedia object from the data and push it into the array
					results.push(new InstagramMedia(data[i]));
				}
			}
 
			if(jsonData.pagination !== undefined)
			{
				var nextUrl = jsonData.pagination.next_url;
				$.getJSON(nextUrl, processData);
			}
			else
			{
				//If any hashtags are defined, filter the results
				if (hashtags !== undefined) {
					results = filterHashtags(results, hashtags);
				}

				//If any usernames are defined, filter the results
				if (usernames !== undefined) {
					results = filterUsers(results, usernames);
				}

				//Remove duplicates from the results
				results = removeDuplicates(results);

				that.lastResults = results;
				//This was the last call to process data. Call the callback method
				callback(results);

			}
		}
	}

	//Takes an array and returns all the items in that array that have the given tag
	//	- mediaArray -> The array to search through
	//	- hashtags -> An array of hashtags to filter by
	function filterHashtags (mediaArray, hashtags) {
		//If the hashtags parameter is defined
		if(hashtags !== undefined && mediaArray !== undefined){
			
			var filteredResults = [];
			
			//Check if the mediaArray has hashtags
			for (var i = 0; i < mediaArray.length; i++) {
				if(mediaArray[i].tags !== undefined && mediaArray[i].tags.length > 0){
					//Check if the media objects tags contains any of the hashtags
					for(var j = 0; j < hashtags.length; j++){
						for (var k = 0; k < mediaArray[i].tags.length; k++) {
							if(hashtags[j] === mediaArray[i].tags[k]){
								//Push the InstagramMedia object into the array
								filteredResults.push(mediaArray[i]);
							}
						}
					}
				}
			}

			//Return the filtered results array
			return filteredResults;
		}
	}

	//Takes an array and returns all the items in that array that have the given tag
	//	- mediaArray -> The array to search through
	//	- usernames -> An array of usernames to filter by
	function filterUsers (mediaArray, usernames) {
		//If the usernames parameter is defined
		if(usernames !== undefined && mediaArray !== undefined){
			
			var filteredResults = [];
			
			//Check if the mediaArray has any photos with the given usernames
			for (var i = 0; i < mediaArray.length; i++) {
				if(mediaArray[i].user !== undefined){
					//Check if the media item was posted by the user
					for(var j = 0; j < usernames.length; j++){
						//Check if the current username is the media results username
						if(usernames[j] === mediaArray[i].user.username){
							//Push the InstagramMedia object into the array
							filteredResults.push(mediaArray[i]);
						}

						//check if the current user is in the in the photo 
						if(mediaArray[i].usersInPhoto !== undefined && mediaArray[i].usersInPhoto.length > 0){
							for (var k = 0; k < mediaArray[i].usersInPhoto.length; k++) {
								//Check if the current userInPhoto has the desired username
								if (usernames[j] === mediaArray[i].usersInPhoto[k].username) {
									filteredResults.push(mediaArray[i]);
								}
							}
						}
					}
				}
			}

			//Return the filtered results array
			return filteredResults;
		} 
	}

	//Create the function for removing duplicates
	//	- mediaArray -> The array to remove duplicates from
	function removeDuplicates (mediaArray) {
		var noDupeArray = [];

		//Loop through every media object
		for (var i = 0; i < mediaArray.length; i++) {
			//Loop over every media object after the current one
			for (var j = i + 1; j < mediaArray.length; j++) {
				//Make sure neither objects are undefined
				if(mediaArray[i] !== undefined && mediaArray[j] !== undefined){
					//If the objects are the same
					if(mediaArray[i].isEqualTo(mediaArray[j]) === true){
						//Mark the seconds one as undefined
						mediaArray[j] = undefined;
					}
				}
			}
		}

		//Loop through and add all the defined values to the new array
		for (var i = 0; i < mediaArray.length; i++) {
			//only get the defined objects
			if(mediaArray[i] !== undefined){
				noDupeArray.push(mediaArray[i]);
			}
		}

		return noDupeArray;
	}
}