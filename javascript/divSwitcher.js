/*
    Filename: divSwitcher.js
    Purpose: Creates and provides easy switching between two divs
    Date: 25/03/2014
    Author: Kierran McPherson
*/

//Declare the constructor for DivSwitcher
//  wrapperDivID -> The main div that the child divs are created in
//  divOneID -> The ID to give to the first child div
//  divTwoID -> The ID to give to the second child div
function DivSwitcher(wrapperDivID, divOneID, divTwoID) {
    var DIV_ONE = 0;
    var DIV_TWO = 1;

    var current = 0;
    var that = this;

    this.wrapperDiv = document.getElementById(wrapperDivID);
    this.divOneID = divOneID;
    this.divOne = createDivOne();
    this.divTwoID = divTwoID;
    this.divTwo = createDivTwo();
    this.swapOut = changeDivs;
    this.toDivOne = switchToDivOne;
    this.toDivTwo = switchToDivTwo;

    adjustDivs();

    //Declare functions

    //Uses jQuery to slide one div in place of another
    function changeDivs() {
        //Create the animation options objects
        var animateOptions = {
            duration: 500, //milliseconds
        };

        //Check which div is being changed
        if (current == DIV_ONE) {
            //Switch between divOne and divTwo
            var divOneCSS = {
                left: (0 - that.divOne.offsetWidth) + "px",
            };

            var divTwoCSS = {
                left: 0 + "px",
            };


            jQuery(that.divOne).animate(divOneCSS, animateOptions);
            jQuery(that.divTwo).animate(divTwoCSS, animateOptions);
            current = DIV_TWO;
        } else {
            //Switch between divTwo and divOne
            var divTwoCSS = {
                left: (that.divOne.offsetWidth) + "px",
            };

            var divOneCSS = {
                left: 0 + "px",
            };

            jQuery(that.divTwo).animate(divTwoCSS, animateOptions);
            jQuery(that.divOne).animate(divOneCSS, animateOptions);
            current = DIV_ONE;
        }
    }

    //Changes to divOne if current is not divOne
    function switchToDivOne () {
        //Create the animation options objects
        var animateOptions = {
            duration: 500, //milliseconds
        };

        //If the current div is not the div we are trying to switch to
        if (current !== DIV_ONE) {
            //Switch from divTwo to divOne
            var divTwoCSS = {
                left: (that.divOne.offsetWidth) + "px",
            };

            var divOneCSS = {
                left: 0 + "px",
            };

            jQuery(that.divTwo).animate(divTwoCSS, animateOptions);
            jQuery(that.divOne).animate(divOneCSS, animateOptions);
            current = DIV_ONE;
        }
        //else don't do anything, divOne is already displayed
    }

    //Changes to divTwo if current is not DivTwo
    function switchToDivTwo () {
        //Create the animation options objects
        var animateOptions = {
            duration: 500, //milliseconds
        };

        //If the current div is not the div we are trying to switch to
        if (current !== DIV_TWO) {
        //Switch between divTwo and divOne
        var divOneCSS = {
                left: (0 - that.divOne.offsetWidth) + "px",
            };

            var divTwoCSS = {
                left: 0 + "px",
            };


            jQuery(that.divOne).animate(divOneCSS, animateOptions);
            jQuery(that.divTwo).animate(divTwoCSS, animateOptions);
            
            current = DIV_TWO;
        }
    }

    //Create divOne inside the existing div
    function createDivOne() {
        //Create the new div to be divOne
        var temp = divOne = document.createElement('div');

        //Add a class to the new node so we can get it later
        temp.id = that.divOneID;

        //Set the offsetHeight and offsetWidth of the unchanged wrapper div
        temp.offsetHeight = that.wrapperDiv.offsetHeight;
        temp.offsetWidth = that.wrapperDiv.offsetWidth;

        //Append the node to the wrapper div
        that.wrapperDiv.appendChild(temp);

        return temp;
    }

    function createDivTwo() {
        //Create the new div to be divTwo
        var temp = document.createElement('div');

        //Add a class to the new node so we can get it later
        temp.id = that.divTwoID;

        //Set the offsetHeight and offsetWidth of the unchanged wrapper div
        temp.offsetHeight = that.wrapperDiv.offsetHeight;
        temp.offsetWidth = that.wrapperDiv.offsetWidth;

        //Append the node to the wrapper div
        that.wrapperDiv.appendChild(temp);

        return temp;
    }

    function adjustDivs() {
        var wrapperLeft = that.wrapperDiv.offsetLeft;
        var wrapperTop = that.wrapperDiv.offsetTop;
        var wrapperHeight = that.wrapperDiv.offsetHeight;
        var wrapperWidth = that.wrapperDiv.offsetWidth;

        //Position and size divOne
        that.divOne.style.position = "relative";
        that.divOne.style.left = 0 + "px";
        that.divOne.style.top = 0 + "px";
        that.divOne.style.width = wrapperWidth + "px";
        that.divOne.style.height = wrapperHeight + "px";

        //Position and size divTwo
        that.divTwo.style.position = "relative";
        //set that.divTwo to be off to the offsetLeft
        that.divTwo.style.left = (0 + wrapperWidth) + "px";
        that.divTwo.style.top = (0 - wrapperHeight) + "px";
        that.divTwo.style.width = wrapperWidth + "px";
        that.divTwo.style.height = wrapperHeight + "px";

        //Change the wrappers overflow settings to be hidden
        that.wrapperDiv.style.overflow = "hidden";
    }
}