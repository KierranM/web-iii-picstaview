var ge;
var divSwitcher;
var geoMath;
var instagramAccessor;
var instagramMediaArray;
var placeMarks;
var streetMarkers;
var earthTimer;
var streetTimer
var dunedin;
var filter;
var latitude;
var longitude;
var panorama;
var sv;

var gallery;

function init ()
{
	$('#pictureDiv').hide();
	sv = new google.maps.StreetViewService();
	var btnFilter = document.getElementById('filter');
	var buttonSwitch = document.getElementById('switch');
	var btnShowImages = document.getElementById('getImages');
	var sliderPrev = document.getElementById('sliderPrev');
	var sliderNext = document.getElementById('sliderNext');
	var pictureDiv = document.getElementById('pictureDiv');
	var gallery = document.getElementById('nearbySlider');
	var clearImages = document.getElementById('clearImages');
	var zoomTo = document.getElementById('zoomTo');
	btnFilter.onclick = filterButtonClick;
	btnShowImages.onclick = showImages;
	sliderPrev.onclick = prevSlide;
	sliderNext.onclick = nextSlide;
	pictureDiv.onclick = hidePictureDiv;
	gallery.onclick = galleryClick;
	clearImages.onclick = clearAll;
	divSwitcher = new DivSwitcher("projectDiv","earthDiv","streetDiv");
	geoMath = new GeoMaths();
	instagramAccessor = new InstagramAccessor("5d87d8c9a88940c19997e9a0fb4c2dea");
	instagramMediaArray = [];
	placeMarks = [];
	streetMarkers = [];
	dunedin = new google.maps.LatLng(-45.861147,170.517619);
	var panoramaOptions = {
		position: dunedin,
		pov: {
			heading: 34,
			pitch: 10,
			zoom: 1
		}
	};
	panorama = new  google.maps.StreetViewPanorama(divSwitcher.divTwo);
	google.maps.event.addListener(panorama, 'pov_changed', 	function () {
		if(streetTimer){
		clearTimeout(streetTimer);
		}
		streetTimer = setTimeout(streetViewZoomChange, 100);
		}
	);	
	google.earth.createInstance(divSwitcher.divOneID, initCB, failureCB);
	//Filtering starts false
	filter = false;
}

//Moves the gallery to the previous slide
function prevSlide (event) {
	event.stopPropagation();
	gallery.navigate('prev');
}

//Moves the gallery to the next slide
function nextSlide (event) {
	event.stopPropagation();
	gallery.navigate('next');
}

//Hides the picture div and deletes the gallery
function hidePictureDiv (event){
	$('#pictureDiv').hide();
	delete gallery;
}

//Clears all markers, placemarks, and images
function clearAll () {
	//loop for every placemark and remove it. Hide the picture div
	$('#pictureDiv').hide();

	var children = ge.getFeatures().getChildNodes();
	for(var i = 0; i < children.getLength(); i++) { 
		var child = children.item(i);
		if(child.getType() == 'KmlPlacemark') {
			ge.getFeatures().removeChild(child);
		}
    }
	placeMarks = [];

	for (var i = 0; i < streetMarkers.length; i++) {
		streetMarkers[i].setMap(null);
	}

	streetMarkers = [];

	instagramMediaArray = [];

	instagramAccessor = new InstagramAccessor("5d87d8c9a88940c19997e9a0fb4c2dea");
}

function galleryClick (event) {
	event.stopPropagation();
}

function streetViewZoomChange()
{
	//Get the zoom level of the panorama
	//If it is less than 1 switch back to earth

	var pov = panorama.getPov();
	var zoomLevel = pov.zoom;

   	if(zoomLevel < 1)
   	{

   		divSwitcher.toDivOne();
   		var pov = panorama.getPov();
   		pov.zoom = 1;
   		panorama.setPov(pov);
   	}
	
}
/*
	Switches between the earth and streetview divs when earth is zoomed to below 500m
*/
function switchWindows()
{
	var camera = ge.getView().copyAsCamera(ge.ALTITUDE_RELATIVE_TO_GROUND);
	var altitude = camera.getAltitude();
	if(altitude <= 500)
	{
		var lat = camera.getLatitude();
		var lon = camera.getLongitude();
		var camLatLng = new google.maps.LatLng(lat,lon);
		sv.getPanoramaByLocation(camLatLng, 50, processSVData);
	}
	
}

/*
	Takes the current location of the Google Earth camera and requests photos around that area
	Currently called on button click
*/
function showImages () {
	var camera = ge.getView().copyAsCamera(ge.ALTITUDE_RELATIVE_TO_GROUND);
	var lat = camera.getLatitude();
	var lon = camera.getLongitude();
	var gpsGrid = geoMath.createGrid(lat, lon, 4,4,1);

	for (var i = 0; i < gpsGrid.length; i++) {
		instagramAccessor.searchForMedia(addPlacemarks, gpsGrid[i].lat, gpsGrid[i].long);
	}
}

/*
	Callback method for the InstagramAccessor.searchForMedia method. This occurs asynchroneously.
	Loops over all the InstagramMedia objects in results and creates markers for them
*/
function addPlacemarks (results) {
	var oldLength = instagramMediaArray.length;
	instagramMediaArray = instagramMediaArray.concat(results);
	instagramMediaArray = instagramAccessor.removeDuplicates(instagramMediaArray);
	
	for (var i = oldLength; i < instagramMediaArray.length; i++) {
		createPlacemark(instagramMediaArray[i]);
	}
	if (filter === true) {
		applyFilter(instagramMediaArray);
	}
}

/*
	Hides all the placemarks on google earth
*/
function hidePlacemarks(){
	//Hide all placemarks
	for (var i = 0; i < placeMarks.length; i++) {
		placeMarks[i].setVisibility(false);
	}
}

/*
	Removes the current filter that is being applied to results
*/
function removeFilter(){
	hidePlacemarks();

	for (var i = 0; i < instagramMediaArray.length; i++) {
		ge.getElementById(instagramMediaArray[i].mediaID).setVisibility(true);
	}

	filter = false;
}

/*
	Called when the filter button is clicked. It either applies or removes a filter
*/
function filterButtonClick() {
	if (filter === false) {
		document.getElementById('filter').value = "Remove Filter";
		applyFilter(instagramMediaArray);
	} else {
		document.getElementById('filter').value = "Apply Filter";
		removeFilter();
	}
}

/*
	Takes the values in the textbox and filters all the currently displayed images by them
*/
function applyFilter (mediaArray)
{
	hidePlacemarks();

	var filterObj = getFilters();

	if (filterObj !== undefined) {
		var filteredResults = [];

		if (filterObj.hashtagFilters !== undefined) {
			filteredResults = instagramAccessor.filterHashtags(mediaArray, filterObj.hashtagFilters);
		}

		if (filterObj.usernameFilters !== undefined) {
			filteredResults = instagramAccessor.filterUsernames(mediaArray, filterObj.usernameFilters);
		}

		filteredResults = instagramAccessor.removeDuplicates(filteredResults);

		for (var i = 0; i < filteredResults.length; i++) {
			ge.getElementById(filteredResults[i].mediaID).setVisibility(true);
		}

		filter = true;
	}
}

//Returns an object containing two arrays
function getFilters () {
	var filters = document.getElementById('txtFilter').value.trim();

	var splitFilters = filters.split(' ');

	if (splitFilters.length > 0) {
		var hashtagFilters = [];
		var usernameFilters = [];

		for (var i = 0; i < splitFilters.length; i++) {
			//If the first character is an @ symbol the value is a username
			if(splitFilters[i][0] === '@'){
				//Push the value to the username array, but take off the @
				usernameFilters.push(splitFilters[i].substring(1));
			} else {
				//If the first character is a # the value is a hashtag
				if (splitFilters[i][0] === '#') {
					//Push the value to the hashtag array, taking off the #
					hashtagFilters.push(splitFilters[i].substring(1));
				}
			}
		}

		//If hashtags is empty set it to be undefined
		if(hashtagFilters.length === 0){
			hashtagFilters = undefined;
		}

		//If no usernames given set it to be undefined
		if (usernameFilters.length === 0) {
			usernameFilters = undefined;
		}

		//Create the filter object
		var filterObj = {
			hashtagFilters: hashtagFilters,
			usernameFilters: usernameFilters
		};

		//Return the filter object
		return filterObj;
	} else {
		//no text was entered into the box so return undefined
		return undefined;
	}
}

/*
	Creates a Google Earth Placemark and a Google Maps marker for the given InstagramMedia object
*/
function createPlacemark (instagramMediaObject) {
	//Create the place mark
	var placemark = ge.createPlacemark(instagramMediaObject.mediaID);
	//Create the placemarks point
	var point = ge.createPoint('');
	//Set the points values
	point.setLatitude(instagramMediaObject.latitude);
	point.setLongitude(instagramMediaObject.longitude);

	//Set the placemarks geometry to the point
	placemark.setGeometry(point);

	//Create the icon for the marker
	var icon = ge.createIcon('');
	icon.setHref(instagramMediaObject.thumbnail.url);
	//create a new style
	var style = ge.createStyle('');
	//apply the icon to the style
	style.getIconStyle().setIcon(icon); 
	//apply the style to the placemark
	placemark.setStyleSelector(style); 
		
	//Add the placemark to the placemark array
	placeMarks.push(placemark);

	//Create the streetview marker
	//Create the icon object
	var iconDetails = {
		url: instagramMediaObject.image.url,
		scaledSize: new google.maps.Size(300, 300)
	};

	var markerLatLng = new google.maps.LatLng(instagramMediaObject.latitude, instagramMediaObject.longitude);

	var markerOptions = {
		icon: iconDetails,
		position: markerLatLng,
		title: instagramMediaObject.caption,
		id: instagramMediaObject.mediaID
	};

	var marker = new google.maps.Marker(markerOptions);

	marker.setMap(panorama);
	streetMarkers.push(marker);

	//Add the placemark to the map
	ge.getFeatures().appendChild(placemark);
	google.earth.addEventListener(placemark, 'click', placemarkClick);
	google.maps.event.addListener(marker, 'click', placemarkClick);
}
 
 function placemarkClick (event) {
 	var pmark = event.getTarget();
 	var id = 0;
 	if(pmark.getType() == 'KmlPlacemark'){
 		id = pmark.getId();
 	}
 	else{
 		id = pmark.id;
 	}
 	displayImageInBox(id);
 }

 function displayImageInBox (mediaID) {
 	for (var i = 0; i < instagramMediaArray.length; i++)
 	{
 			var image = instagramMediaArray[i];
 		if (image.mediaID === mediaID)
 		{
 			setImageDetails(image);

 			var galleryImages = [];
 			var galleryImagesLocations = [];
 			galleryImages.push([image.thumbnail.url, '', '', image.user.username]);
 			galleryImagesLocations.push(i);

 			//Get nearby images (within 500m)
 			for (var j = 0; j < instagramMediaArray.length; j++) {
 				if (instagramMediaArray[j] !== image) {
 					var distance = geoMath.distanceBetween(image.latitude, image.longitude, instagramMediaArray[j].latitude, instagramMediaArray[j].longitude);
 					if (distance <= 0.5) {
 						galleryImages.push([instagramMediaArray[j].thumbnail.url, '', '', instagramMediaArray[j].user.username]);
 						galleryImagesLocations.push(j);
 					}
 				}
 			}

 			gallery = new simpleGallery({
					wrapperid: "nearbySlider", //ID of main gallery container,
					dimensions: [150, 150], //width/height of gallery in pixels. Should reflect dimensions of the images exactly
					imagearray: galleryImages,
					autoplay: [false, 2500, 2], //[auto_play_boolean, delay_btw_slide_millisec, cycles_before_stopping_int]
					persist: false,
					preloadfirst:false, //v1.4 option on whether slideshow should only start after all images have preloaded
					fadeduration: 500, //transition duration (milliseconds)
					oninit:function(){ //event that fires when gallery has initialized/ ready to run
						//Keyword "this": references current gallery instance (ie: try this.navigate("play/pause")
					},
					onslide:function(curslide, i){ //event that fires after each slide is shown
						//Keyword "this": references current gallery instance
						//curslide: returns DOM reference to current slide's DIV (ie: try alert(curslide.innerHTML)
						//i: integer reflecting current image within collection being shown (0=1st image, 1=2nd etc)
						//Find the image within the media array
						var currImage = instagramMediaArray[galleryImagesLocations[i]];
						setImageDetails(currImage);
						var nextImagePos = (i + 1)%galleryImages.length;
						var prevImagePos = (i - 1);
						if (prevImagePos < 0) {
							prevImagePos = galleryImages.length - 1;
						}
						document.getElementById('sliderNext').style.backgroundImage = 'url("' + instagramMediaArray[galleryImagesLocations[nextImagePos]].thumbnail.url + '")';
						document.getElementById('sliderPrev').style.backgroundImage = 'url("' + instagramMediaArray[galleryImagesLocations[prevImagePos]].thumbnail.url + '")';
					}
				});

 			
 			$('#pictureDiv').show();
 		}
 	}
 }

 function setImageDetails (image) {
 	document.getElementById("imageCaption").textContent = image.caption;
 	document.getElementById("usernameInfo").textContent = image.user.username;
 	document.getElementById("fullnameInfo").textContent = image.user.fullName;
 	var tags = [];
 	for(var j = 0; j < image.tags.length; j++){
 		tags.push('#' + image.tags[j]);
 	}
 	document.getElementById("hashtagsInfo").textContent = tags.join(' ');
 	document.getElementById("likesInfo").textContent = image.likes.length;
 	document.getElementById("dateInfo").textContent = image.humanReadableTime;
 	document.getElementById("latitudeInfo").textContent = image.latitude;
 	document.getElementById("longitudeInfo").textContent = image.longitude;
 	document.getElementById("photo").src=image.image.url;
 }

function initCB(instance) 
{
	ge = instance;
	var lookAt = ge.createLookAt('');
	var camera = instance.getView().copyAsCamera(ge.ALTITUDE_RELATIVE_TO_GROUND);	
	ge.getWindow().setVisibility(true);
	ge.getLayerRoot().enableLayerById(ge.LAYER_BUILDINGS, true);
	ge.getOptions().setStatusBarVisibility(true);
	ge.getOptions().setScaleLegendVisibility(true);
	//ge.getView().setAbstractView(camera);
	//var lat = camera.getLatitude();
	//var lon = camera.getLongitude();
	lookAt.setLatitude(-45.879017);
	lookAt.setLongitude(170.517619);
	lookAt.setRange(16000.0); //default is 0.0
	ge.getView().setAbstractView(lookAt);
	
	google.earth.addEventListener(ge.getView(), 'viewchangeend', function(){
		if(earthTimer){
		clearTimeout(earthTimer);
		}
		earthTimer = setTimeout(switchWindows, 200);
		}
	);
	//alert(panorama.zoom);

}

function processSVData(data, status) {
  	if (status == google.maps.StreetViewStatus.OK) {
    	panorama.setPano(data.location.pano);
    	panorama.setPov({
    	  heading: 270,
    	  pitch: 0,
    	  zoom: 1
    	
    	});

    	var camera = ge.getView().copyAsCamera(ge.ALTITUDE_RELATIVE_TO_GROUND);
		divSwitcher.toDivTwo();
		camera.setAltitude(1000);
		ge.getView().setAbstractView(camera);	
	}

}

function failureCB(errorCode) 
{

} 


google.setOnLoadCallback(init);
//window.onload = init;
