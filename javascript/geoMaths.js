/*
	Filename: geoMaths.js
	Purpose: Provides methods for performing mathematics on latitude and longitude pairs
	Date: 30/03/2014
	Author: Kierran McPherson
	Attributions: Calculations adapted from http://www.movable-type.co.uk/scripts/latlong.html
*/

//Declare the GeoMaths constructor
function GeoMaths () {
	var that = this;
	var EARTH_RAD = 6371;
	this.NORTH = 0;
	this.NORTH_EAST = 45;
	this.EAST = 90;
	this.SOUTH_EAST = 135;
	this.SOUTH = 180;
	this.SOUTH_WEST = 225;
	this.WEST = 270;
	this.NORTH_WEST = 315;

	this.createGrid = createGrid;
	this.calculateCoord = calculateCoord;
	this.distanceBetween = distanceBetween;

	function convertToRad (number) {
		return number * (Math.PI/180);
	}

	function convertToDegrees (radians) {
		return radians * (180/Math.PI);
	}

	//Declare the functions

	function createGrid (centerLat, centerLong, gridRows, gridCols, stepSize) {
		//If the number of rows or columns is not even, increase them by one
		if (gridRows % 2 !== 0) {
			gridRows++;
		}

		if (gridCols % 2 !== 0) {
			gridCols++;
		}

		var distanceToLeftEdge = stepSize * (gridCols/2);
		var farLeft = calculateCoord(centerLat, centerLong, that.WEST, distanceToLeftEdge);
		var distanceToTop = stepSize * (gridRows/2);
		var topLeft = calculateCoord(farLeft.lat, farLeft.long, that.NORTH, distanceToTop);

		var firstGridCoord = calculateCoord(topLeft.lat, topLeft.long, that.SOUTH_EAST, (stepSize/2));

		var rowLat = firstGridCoord.lat;
		var colLong = firstGridCoord.long;
		var gridCoords = [];
		for (var i = 0; i < gridRows; i++) {
			var rowStartLong = colLong;
			for (var j = 0; j < gridCols; j++) {
				gridCoords.push({
					"lat": rowLat,
					"long": colLong
				});

				colLong = calculateCoord(rowLat, colLong, that.EAST, stepSize).long;
			}
			var temp = calculateCoord(rowLat, rowStartLong, that.SOUTH, stepSize);
			rowLat = temp.lat;
			colLong = temp.long;
		}

		return gridCoords;
	}

	//Calculates a latitude and longitude of a point given a start coordinate, a bearing, and a distance in meters
	function calculateCoord (lat, long, bearing, distance) {
		var latR = convertToRad(lat);
		var longR = convertToRad(long);
		var bearingR = convertToRad(bearing);
		var angularDist = distance/EARTH_RAD;

		var lat2 = Math.asin( Math.sin(latR)*Math.cos(distance/EARTH_RAD) + Math.cos(latR)*Math.sin(distance/EARTH_RAD)*Math.cos(bearingR));
		var long2 = longR + Math.atan2(Math.sin(bearingR)*Math.sin(distance/EARTH_RAD)*Math.cos(latR), Math.cos(distance/EARTH_RAD)-Math.sin(latR)*Math.sin(lat2));
		long2 = (long2+3*Math.PI) % (2*Math.PI) - Math.PI;  // normalise to -180..+180º
		var returnObject = {
			"lat": convertToDegrees(lat2),
			"long": convertToDegrees(long2)
		};

		return returnObject;
	}

	function distanceBetween (lat1, long1, lat2, long2) {
		var lat1R = convertToRad(lat1);
		var long1R = convertToRad(long1);
		var lat2R = convertToRad(lat2);
		var long2R = convertToRad(long2);

		var dLat = lat2R - lat1R;
		var dLon = long2R - long1R;

		var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1R) * Math.cos(lat2R) * Math.sin(dLon/2) * Math.sin(dLon/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		var d = EARTH_RAD * c;

		return d;
	}
}