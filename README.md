PicstaView:

PicstaView is an interactive look at the world of Instagram. 

The majority of photos uploaded to Instagram are tagged with the GPS coordinates 
of the location they were taken at.
Using this GPS information PicstaView shows them on a 3D representation of our world using the Google Earth
Web API. These appear as small thumbnails that expand to display the full image when hovered.
If the user clicks an image then a gallery opens with a selection of images from either the
nearby area or with similar hashtags.

If the user zooms in far enough the Google Earth display will be swapped for a street view display.
In this mode the user will see the same images but as though they were looking at them from the nearest road.

The user can also filter images by Instagram username and also image hashtags.
